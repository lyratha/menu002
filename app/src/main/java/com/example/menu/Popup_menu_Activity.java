package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

public class Popup_menu_Activity extends AppCompatActivity {
Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_menu_);
        button=findViewById(R.id.bt_popup);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu=new PopupMenu(getBaseContext(),button);
                getMenuInflater().inflate(R.menu.menu,menu.getMenu());
                menu.show();
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.cambodia:
                                Toast.makeText(getBaseContext(),item.getTitle(),Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.thailand:
                                Toast.makeText(getBaseContext(),item.getTitle(),Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.vietnam:
                                Toast.makeText(getBaseContext(),item.getTitle(),Toast.LENGTH_SHORT).show();
                                break;
                        }

                        return false;
                    }
                });
            }
        });
    }
}
